package org.dizitart.no2.example.hos.slice;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Button;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.event.ChangeListener;
import org.dizitart.no2.example.hos.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.dizitart.no2.util.Iterables;

import java.util.List;

public class ExampleAbilitySlice extends AbilitySlice {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "Nitrite");
    ChangeListener listener;
    private Nitrite db;
    private ObjectRepository<User> repository;
    UserAdapter adapter;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_example);
        if (db == null) {
            String fileName = getFilesDir().getPath() + "/test.db";
            db = Nitrite.builder()
                    .filePath(fileName)
                    .openOrCreate("test-user", "test-password");
            repository = db.getRepository(User.class);
            Ability that = this.getAbility();
            listener = changeInfo -> {
                HiLog.info(LABEL,"changeInfo");
                try{
                    that.getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            refreshListContainer();
                        }
                    });
                }catch (Exception e){
                    HiLog.info(LABEL,"changeInfo err%{public}s.",e.getMessage());
                }
            };
            repository.register(listener);
        }
        initListContainer();
        Button btnAddUser = (Button) findComponentById(ResourceTable.Id_add_user);
        btnAddUser.setClickedListener(component -> {
            addUser();
        });
        Button btnFlushUser = (Button) findComponentById(ResourceTable.Id_flush_user);
        btnFlushUser.setClickedListener(component -> {
            flushUsers();
        });
    }
    private void addUser() {
        User user = new User("user "+System.currentTimeMillis(), "");
        repository.insert(user);
    }

    private void flushUsers() {
        repository.remove(ObjectFilters.ALL);
    }
    @Override
    public void onActive() {
        super.onActive();
        repository.register(listener);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        repository.deregister(listener);
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
    private void initListContainer() {
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
        List<User> list = getData();
        adapter = new UserAdapter(list, this);
        listContainer.setItemProvider(adapter);
        listContainer.setItemClickedListener((container, component, position, id) -> {
            User item = (User) listContainer.getItemProvider().getItem(position);
            new ToastDialog(this)
                    .setText("you clicked:" + item.getUsername())
                    // Toast显示在界面中间
                    .setAlignment(LayoutAlignment.CENTER)
                    .show();
        });
    }
    private void refreshListContainer() {
        HiLog.info(LABEL,"refreshListContainer>>>>>1");
        List<User> list = getData();
        HiLog.info(LABEL,"refreshListContainer>>>>>2");
        adapter.setList(list);
        HiLog.info(LABEL,"refreshListContainer>>>>>3");
        adapter.notifyDataChanged();
        HiLog.info(LABEL,"refreshListContainer>>>>>4");
    }
    private List<User> getData() {
        Iterable<User> users = repository.find().project(User.class);
        List<User> list =  Iterables.toList(users);
        return  list;
    }
}
