package org.dizitart.no2.example.hos.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import org.dizitart.no2.example.hos.ResourceTable;

import java.util.List;
public class UserAdapter extends BaseItemProvider {
    private List<User> list;

    public void setList(List<User> list) {
        this.list = list;
    }

    private AbilitySlice slice;
    public UserAdapter(List<User> list, AbilitySlice slice) {
        this.list = list;
        this.slice = slice;
    }
    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }
    @Override
    public Object getItem(int position) {
        if (list != null && position >= 0 && position < list.size()){
            return list.get(position);
        }
        return null;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_row_user, null, false);
        } else {
            cpt = convertComponent;
        }
        User sampleItem = list.get(position);
        Text text = (Text) cpt.findComponentById(ResourceTable.Id_username);
        text.setText(sampleItem.getUsername());
        return cpt;
    }
}