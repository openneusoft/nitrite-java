package org.dizitart.no2.example.hos.slice;

import org.dizitart.no2.Document;
import org.dizitart.no2.mapper.Mappable;
import org.dizitart.no2.mapper.NitriteMapper;

public class User implements Mappable {
    private String id;
    private String username;
    private String email;

    // needed for deserialization
    public User(){}

    public User(String username, String email) {
        this.username = username;
        this.email = email;
        this.id = ""+System.currentTimeMillis();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @Override
    public Document write(NitriteMapper nitriteMapper) {
        Document document = new Document();
        document.put("id", id);
        document.put("username", username);
        document.put("email", email);
        return document;
    }

    @Override
    public void read(NitriteMapper nitriteMapper, Document document) {
        id = (String) document.get("id");
        username = (String) document.get("username");
        email = (String) document.get("email");
    }
}
